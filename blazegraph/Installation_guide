<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>Installation guide - Blazegraph</title>
<meta name="generator" content="MediaWiki 1.24.2" />
<link rel="shortcut icon" href="/wiki/images/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/wiki/opensearch_desc.php" title="Blazegraph (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://wiki.blazegraph.com/wiki/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/index.php/Installation_guide" />
<link rel="alternate" type="application/atom+xml" title="Blazegraph Atom feed" href="/wiki/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://wiki.blazegraph.com/wiki/load.php?debug=false&amp;lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.content.externallinks%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.blazegraph.styles&amp;only=styles&amp;skin=blazegraph&amp;*" />
<!--[if IE 6]><link rel="stylesheet" href="/wiki/skins/Blazegraph/IE60Fixes.css?303" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/wiki/skins/Blazegraph/IE70Fixes.css?303" media="screen" /><![endif]-->
<link rel="stylesheet" href="/wiki/skins/Blazegraph/blazegraph.css?303" media="screen" />
<link rel="stylesheet" href="/wiki/skins/Blazegraph/bootstrap/css/bootstrap.min.css?303" media="screen" />
<link rel="stylesheet" href="/wiki/skins/Blazegraph/bootstrap/css/ionicons.css?303" media="screen" /><meta name="ResourceLoaderDynamicStyles" content="" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: bigdata_wiki:resourceloader:filter:minify-css:7:4f6f7aea4b52734f22b4087a0ffe4141 */</style>
<script>if(window.mw){
mw.loader.state({"skins.blazegraph.js":"loading"});
}</script>
<script src="https://wiki.blazegraph.com/wiki/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=blazegraph&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Installation_guide","wgTitle":"Installation guide","wgCurRevisionId":16817,"wgRevisionId":16817,"wgArticleId":11091,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Installation_guide","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"editfont":"default","editondblclick":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":1,"extendwatchlist":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nickname":"","norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"rcdays":7,"rclimit":50,"rows":25,"showhiddencats":0,"shownumberswatching":1,"showtoolbar":1,"skin":"blazegraph","stubthreshold":0,"thumbsize":5,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":1,"watchdefault":1,"watchdeletion":0,"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"watchrollback":0,
"wllimit":250,"useeditwarning":1,"prefershttps":1,"language":"en","variant-gan":"gan","variant-iu":"iu","variant-kk":"kk","variant-ku":"ku","variant-shi":"shi","variant-sr":"sr","variant-tg":"tg","variant-uz":"uz","variant-zh":"zh","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"searchNs274":false,"searchNs275":false,"variant":"en"});},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{});
/* cache key: bigdata_wiki:resourceloader:filter:minify-js:7:2a72448c6c32038cae5bb2ed412c8ce5 */
}</script>
<script>if(window.mw){
document.write("\u003Cscript src=\"https://wiki.blazegraph.com/wiki/load.php?debug=false\u0026amp;lang=en\u0026amp;modules=skins.blazegraph.js\u0026amp;only=scripts\u0026amp;skin=blazegraph\u0026amp;*\"\u003E\u003C/script\u003E");
}</script>
<script>if(window.mw){
mw.loader.load(["mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax","ext.GoogleLogin.style","skins.blazegraph.js"]);
}</script>
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-Installation_guide skin-blazegraph action-view">

 <!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="row marketing">
			<!-- 		navbar-collapse collapse -->

			<div class="col-lg-2 col-lg-offset-1 col-md-3 col-sm-3 col-xs-3"
				style="left: -10px;">
				<div style="overflow: visible; height: 90px;">
					<a href="/wiki/"><img src="/wiki/images/blazegraph_logo.png" width=274px height=134px
						style="padding-top: 0px; padding-bottom: 0px;"></a>
				</div>
			</div>

			<div class="col-lg-7 col-md-9 col-sm-9 col-xs-9">
				<div class="custom-search-input Search" style="width:145px;position:absolute; right:350px; top:17px;">
					<form class="form-inline">
						<div class="form-group">
									<input type='hidden' name="title"
										value="Special:Search" />

									<div class="input-group">
					<span class="input-group-btn">
						<button class="btn" type="submit"><span class="ion-ios-search"></span> </button>
					</span>
									<input type="search" name="search" placeholder="Search" title="Search Blazegraph [f]" accesskey="f" id="searchInput" class="search-query form-control" aria-describedby="searchGoButton" />							
							</form>
				</div>
				</div>
				</div>
			<div id="navbar">
					<ul class="social_icons_wp nav navbar-nav navbar-right" style="padding-right:5px">
			<li>
			    <a class="btn btn-block btn-social btn-github" href="https://www.facebook.com/blazegraph">
				<span class="ion-social-facebook"></span>
			    </a>
			</li>
			<li>
			    <a class="btn btn-block btn-social btn-github" href="https://twitter.com/blazegraph">
				<span class="ion-social-twitter"></span>
			    </a>
			</li>
			<li>
			    <a class="btn btn-block btn-social btn-github" href="https://www.linkedin.com/company/systap-llc">
				<span class="ion-social-linkedin-outline"></span> 
			    </a>
			</li>
			<div class="clear"></div>
		    </ul>
		</div>
		<br><br><br>
		<!--/.nav-collapse -->
				<div id="navbar">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
			<a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-expanded="false">
					<div style="position:relative;top:-6px;">
									<i class="ion-android-home"></i>
									Home <span class="caret"></span>
					   	</div>
								</a>
							<ul class="dropdown-menu" role="menu"
								>
					<li><a href="/wiki/">Wiki Home</a></li>
								<li><a href="http://blazegraph.com/">Blazegraph</a></li>
								<li><a href="http://blog.blazegraph.com/">Blog</a></li>
								<li><a href="http://blazegraph.com/docs/api/index.html">Javadoc/API</a></li>
				</ul>
			</li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-expanded="false">Personal
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu"
								>
																	<li id="pt-login"><a href="/wiki/index.php?title=Special:GoogleLogin&amp;returnto=Installation+guide" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Login with Google</a></li>															</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false">Page<span
								class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
							
							<li id="ca-nstab-main" class="selected"><a href="/wiki/index.php/Installation_guide" title="View the content page [c]" accesskey="c">Page</a></li>
							<li id="ca-talk" class="new"><a href="/wiki/index.php?title=Talk:Installation_guide&amp;action=edit&amp;redlink=1" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li>
							<li id="ca-viewsource"><a href="/wiki/index.php?title=Installation_guide&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li>
							<li id="ca-history"><a href="/wiki/index.php?title=Installation_guide&amp;action=history" rel="archives" title="Past revisions of this page [h]" accesskey="h">History</a></li>							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-expanded="false">Tools
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
																	<li id="t-whatlinkshere"><a href="/wiki/index.php/Special:WhatLinksHere/Installation_guide" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li>																	<li id="t-recentchangeslinked"><a href="/wiki/index.php/Special:RecentChangesLinked/Installation_guide" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li>																	<li id="t-specialpages"><a href="/wiki/index.php/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li>																	<li id="t-print"><a href="/wiki/index.php?title=Installation_guide&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li>																	<li id="t-permalink"><a href="/wiki/index.php?title=Installation_guide&amp;oldid=16817" title="Permanent link to this revision of the page">Permanent link</a></li>																	<li id="t-info"><a href="/wiki/index.php?title=Installation_guide&amp;action=info">Page information</a></li>															</ul></li>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--a class="navbar-brand" href="#">Blazegraph</a-->
		</div>
	</div>
</nav>
<div class="container-fluid">

	<div class="row marketing">

		<div class="col-lg-2 col-lg-offset-1 col-md-3 col-sm-3 col-xs-3">
			<div class="accordion sidebar" id="accordion1">
				<div class="accordion-group">
						<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Introduction">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Introduction </a>
</div>
<div id="p-Introduction" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-About-Blazegraph"><a href="/wiki/index.php/About_Blazegraph">About Blazegraph</a></div><div id="n-Concepts"><a href="/wiki/index.php/Concepts">Concepts</a></div><div id="n-Feature-Matrix"><a href="/wiki/index.php/FeatureMatrix">Feature Matrix</a></div><div id="n-Roadmap"><a href="/wiki/index.php/Roadmap">Roadmap</a></div><div id="n-Support"><a href="/wiki/index.php/Support">Support</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse"  "aria-expanded"="true"		data-parent="#accordion1" href="#p-Getting_Started">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Getting Started </a>
</div>
<div id="p-Getting_Started" class="accordion-body collapse in" "aria-expanded"="true"	>
			<div id="n-Quick-Start"><a href="/wiki/index.php/Quick_Start">Quick Start</a></div><span class="selected-toc">
									<div id="n-Installation-guide"><a href="/wiki/index.php/Installation_guide">Installation guide</a></div></span><div id="n-Configuring-Blazegraph"><a href="/wiki/index.php/Configuring_Blazegraph">Configuring Blazegraph</a></div><div id="n-Client-Libraries"><a href="/wiki/index.php/Client_Libraries">Client Libraries</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Tutorials">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Tutorials </a>
</div>
<div id="p-Tutorials" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-First-Application-Tutorial"><a href="/wiki/index.php/First_Application_Tutorial">First Application Tutorial</a></div><div id="n-Sesame-API-Tutorial"><a href="/wiki/index.php/Sesame_API_Tutorial">Sesame API Tutorial</a></div><div id="n-SOLR-External-Full-Text-Search"><a href="/wiki/index.php/SOLR_External_Fulltext_Search">SOLR External Full Text Search</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-SPARQL_Tips_and_Tricks">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 SPARQL Tips and Tricks </a>
</div>
<div id="p-SPARQL_Tips_and_Tricks" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-Update-Performance"><a href="/wiki/index.php/SPARQL_Update_Performance">Update Performance</a></div><div id="n-Order-Matters"><a href="/wiki/index.php/SPARQL_Order_Matters">Order Matters</a></div><div id="n-Bottom-Up-Semantics"><a href="/wiki/index.php/SPARQL_Bottom_Up_Semantics">Bottom Up Semantics</a></div><div id="n-Tips-and-Tricks"><a href="/wiki/index.php/SPARQL_Tips_and_Tricks">Tips and Tricks</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-APIs">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 APIs </a>
</div>
<div id="p-APIs" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-NanoSparqlServer"><a href="/wiki/index.php/NanoSparqlServer">NanoSparqlServer</a></div><div id="n-REST-API"><a href="/wiki/index.php/REST_API">REST API</a></div><div id="n-JAVA-Client-API"><a href="/wiki/index.php/JAVA_Client_API">JAVA Client API</a></div><div id="n-Client-Libraries"><a href="/wiki/index.php/Client_Libraries">Client Libraries</a></div><div id="n-Notes-on-compliance-with-the-Sesame-API"><a href="/wiki/index.php/SesameAPICompliance">Notes on compliance with the Sesame API</a></div><div id="n-Apache-Tinkerpop-3"><a href="https://github.com/blazegraph/tinkerpop3/#blazegraph-tinkerpop3-implementation-blazegraph-gremlin" rel="nofollow">Apache Tinkerpop 3</a></div><div id="n-Using-Blueprints-with-Blazegraph"><a href="/wiki/index.php/Using_Blueprints_with_Blazegraph">Using Blueprints with Blazegraph</a></div><div id="n-Using-Bigdata-with-the-OpenRDF-Sesame-HTTP-Server"><a href="/wiki/index.php/Using_Bigdata_with_the_OpenRDF_Sesame_HTTP_Server">Using Bigdata with the OpenRDF Sesame HTTP Server</a></div><div id="n-Blazegraph-FAQ"><a href="/wiki/index.php/Blazegraph_FAQ">Blazegraph FAQ</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Sample_Applications">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Sample Applications </a>
</div>
<div id="p-Sample_Applications" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-Sesame-API-embedded-mode"><a href="/wiki/index.php/Sesame_API_embedded_mode">Sesame API embedded mode</a></div><div id="n-Sesame-API-remote-mode"><a href="/wiki/index.php/Sesame_API_remote_mode">Sesame API remote mode</a></div><div id="n-Blueprints-API-embedded-mode"><a href="/wiki/index.php/Blueprints_API_embedded_mode">Blueprints API embedded mode</a></div><div id="n-Blueprints-API-remote-mode"><a href="/wiki/index.php/Blueprints_API_remote_mode">Blueprints API remote mode</a></div><div id="n-RDR"><a href="/wiki/index.php/RDR">RDR</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-High_Availability">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 High Availability </a>
</div>
<div id="p-High_Availability" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-HAJournalServer"><a href="/wiki/index.php/HAJournalServer">HAJournalServer</a></div><div id="n-HALoadBalancer"><a href="/wiki/index.php/HALoadBalancer">HALoadBalancer</a></div><div id="n-Group-Commit"><a href="/wiki/index.php/GroupCommit">Group Commit</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Scaleout">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Scaleout </a>
</div>
<div id="p-Scaleout" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-Cluster-Guide"><a href="/wiki/index.php/ClusterGuide">Cluster Guide</a></div><div id="n-Cluster-Startup-FAQ"><a href="/wiki/index.php/ClusterStartupFAQ">Cluster Startup FAQ</a></div><div id="n-Cluster-Setup-Guide-Fedora-10"><a href="/wiki/index.php/ClusterSetupGuide">Cluster Setup Guide Fedora 10</a></div><div id="n-Cluster-Setup-Guide-CentOS-5.3.2F5.4"><a href="/wiki/index.php/ClusterSetupGuide_CentOS_5_3/5_4">Cluster Setup Guide CentOS 5.3/5.4</a></div><div id="n-MiniCluster"><a href="/wiki/index.php/MiniCluster">MiniCluster</a></div><div id="n-SingleMachineCluster"><a href="/wiki/index.php/SingleMachineCluster">SingleMachineCluster</a></div><div id="n-ScaleOutCI"><a href="/wiki/index.php/ScaleOutCI">ScaleOutCI</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-SPARQL_extensions">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 SPARQL extensions </a>
</div>
<div id="p-SPARQL_extensions" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-Full-Text-Search"><a href="/wiki/index.php/FullTextSearch">Full Text Search</a></div><div id="n-External-Full-Text-Search"><a href="/wiki/index.php/ExternalFullTextSearch">External Full Text Search</a></div><div id="n-GeoSpatial-Search"><a href="/wiki/index.php/GeoSpatial">GeoSpatial Search</a></div><div id="n-Analytic-Query"><a href="/wiki/index.php/AnalyticQuery">Analytic Query</a></div><div id="n-Virtual-Graphs"><a href="/wiki/index.php/VirtualGraphs">Virtual Graphs</a></div><div id="n-Query-Hints"><a href="/wiki/index.php/QueryHints">Query Hints</a></div><div id="n-Named-Subquery"><a href="/wiki/index.php/NamedSubquery">Named Subquery</a></div><div id="n-SPARQL-Update"><a href="/wiki/index.php/SPARQL_Update">SPARQL Update</a></div><div id="n-Federated-Query"><a href="/wiki/index.php/FederatedQuery">Federated Query</a></div><div id="n-Stored-Query"><a href="/wiki/index.php/StoredQuery">Stored Query</a></div><div id="n-Property-Paths"><a href="/wiki/index.php/PropertyPaths">Property Paths</a></div><div id="n-Custom-Function"><a href="/wiki/index.php/CustomFunction">Custom Function</a></div><div id="n-InlineIVs"><a href="/wiki/index.php/InlineIVs">InlineIVs</a></div><div id="n-Linked-Data"><a href="/wiki/index.php/LinkedData">Linked Data</a></div><div id="n-Inference-and-Truth-Maintenance"><a href="/wiki/index.php/InferenceAndTruthMaintenance">Inference and Truth Maintenance</a></div><div id="n-Reification-Done-Right"><a href="/wiki/index.php/Reification_Done_Right">Reification Done Right</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Graph_Mining">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Graph Mining </a>
</div>
<div id="p-Graph_Mining" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-RDF-GAS-API"><a href="/wiki/index.php/RDF_GAS_API">RDF GAS API</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Optimizations_and_benchmarking">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Optimizations and benchmarking </a>
</div>
<div id="p-Optimizations_and_benchmarking" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-Performance-Optimization"><a href="/wiki/index.php/PerformanceOptimization">Performance Optimization</a></div><div id="n-IO-Optimization"><a href="/wiki/index.php/IOOptimization">IO Optimization</a></div><div id="n-Query-Optimization"><a href="/wiki/index.php/QueryOptimization">Query Optimization</a></div><div id="n-Using-Explain"><a href="/wiki/index.php/Explain">Using Explain</a></div><div id="n-SPARQL-Benchmarks"><a href="/wiki/index.php/SPARQL_Benchmarks">SPARQL Benchmarks</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Deeper_Background">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Deeper Background </a>
</div>
<div id="p-Deeper_Background" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-Standalone-Guide"><a href="/wiki/index.php/StandaloneGuide">Standalone Guide</a></div><div id="n-BTree-Guide"><a href="/wiki/index.php/BTreeGuide">BTree Guide</a></div><div id="n-Tx-Guide"><a href="/wiki/index.php/TxGuide">Tx Guide</a></div><div id="n-Striterators"><a href="/wiki/index.php/Striterators">Striterators</a></div><div id="n-RWStore"><a href="/wiki/index.php/RWStore">RWStore</a></div><div id="n-Memory-Manager"><a href="/wiki/index.php/MemoryManager">Memory Manager</a></div><div id="n-Retention-History"><a href="/wiki/index.php/RetentionHistory">Retention History</a></div><div id="n-Unicode"><a href="/wiki/index.php/Unicode">Unicode</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Developers">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Developers </a>
</div>
<div id="p-Developers" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-Maven-Repository"><a href="/wiki/index.php/MavenRepository">Maven Repository</a></div><div id="n-Client-Libraries"><a href="/wiki/index.php/Client_Libraries">Client Libraries</a></div><div id="n-Maven-Notes"><a href="/wiki/index.php/MavenNotes">Maven Notes</a></div><div id="n-Code-Examples"><a href="/wiki/index.php/Code_Examples">Code Examples</a></div><div id="n-Submitting-Bugs"><a href="/wiki/index.php/Submitting_Bugs">Submitting Bugs</a></div><div id="n-Contributors"><a href="/wiki/index.php/Contributors">Contributors</a></div><div id="n-ReleaseGuide"><a href="/wiki/index.php/ReleaseGuide">ReleaseGuide</a></div><div id="n-Branches"><a href="/wiki/index.php/Branches">Branches</a></div><div id="n-Javadoc.2FAPI"><a href="http://www.blazegraph.com/docs/api/" rel="nofollow">Javadoc/API</a></div><div id="n-Common-Problems"><a href="/wiki/index.php/CommonProblems">Common Problems</a></div>			
		</div>
<div class="accordion-heading dropdown"
	>
	<a class="accordion-toggle" data-toggle="collapse" 		data-parent="#accordion1" href="#p-Operations_procedures">
		<div class="pull-left"
			style="padding-bottom: 5px; padding-right: 3px;">
			<span class="caret caret-right"></span>
		</div>
				 Operations procedures </a>
</div>
<div id="p-Operations_procedures" class="accordion-body collapse " style="height: 0;"	>
			<div id="n-Data-Migration"><a href="/wiki/index.php/DataMigration">Data Migration</a></div><div id="n-Bulk-Data-Load"><a href="/wiki/index.php/Bulk_Data_Load">Bulk Data Load</a></div><div id="n-Rebuilding-the-Text-Index"><a href="/wiki/index.php/Rebuild_Text_Index_Procedure">Rebuilding the Text Index</a></div><div id="n-Hardware-Configuration"><a href="/wiki/index.php/Hardware_Configuration">Hardware Configuration</a></div>			
		</div>
								
						</div>
			</div>
			<br>
		</div>
		<div class="col-lg-7 col-md-9 col-sm-9 col-xs-9">

			<div id="content" role="main">
				<!-- 			class="mw-body" -->
				<a id="top"></a>
								<h1 id="firstHeading" class="firstHeading"
					lang="en"><span dir="auto">Installation guide</span></h1>
								<div id="bodyContent" class="mw-body-content">
					<div id="siteSub">From Blazegraph</div>
					<div id="contentSub"
						></div>
										<div id="jump-to-nav" class="mw-jump">Jump to: <a href="#column-one">navigation</a>, <a href="#searchInput">search</a>
					</div>

					<!-- start content -->
					<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div id="toc" class="toc"><div id="toctitle"><h2>Contents</h2></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Java_Requirements"><span class="tocnumber">1</span> <span class="toctext">Java Requirements</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Encoding_Requirements"><span class="tocnumber">1.1</span> <span class="toctext">Encoding Requirements</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-3"><a href="#Download_the_code"><span class="tocnumber">2</span> <span class="toctext">Download the code</span></a>
<ul>
<li class="toclevel-2 tocsection-4"><a href="#GIT"><span class="tocnumber">2.1</span> <span class="toctext">GIT</span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#Eclipse"><span class="tocnumber">2.2</span> <span class="toctext">Eclipse</span></a></li>
<li class="toclevel-2 tocsection-6"><a href="#Other_Environments"><span class="tocnumber">2.3</span> <span class="toctext">Other Environments</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-7"><a href="#Blazegraph_deployment_models"><span class="tocnumber">3</span> <span class="toctext">Blazegraph deployment models</span></a>
<ul>
<li class="toclevel-2 tocsection-8"><a href="#Non-Embedded_Deployment_Models"><span class="tocnumber">3.1</span> <span class="toctext">Non-Embedded Deployment Models</span></a></li>
<li class="toclevel-2 tocsection-9"><a href="#Embedded_RDF.2FGraph_Database"><span class="tocnumber">3.2</span> <span class="toctext">Embedded RDF/Graph Database</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-10"><a href="#Running_Bigdata"><span class="tocnumber">4</span> <span class="toctext">Running Bigdata</span></a></li>
<li class="toclevel-1 tocsection-11"><a href="#Bundling_Bigdata"><span class="tocnumber">5</span> <span class="toctext">Bundling Bigdata</span></a>
<ul>
<li class="toclevel-2 tocsection-12"><a href="#Maven"><span class="tocnumber">5.1</span> <span class="toctext">Maven</span></a></li>
<li class="toclevel-2 tocsection-13"><a href="#Scala"><span class="tocnumber">5.2</span> <span class="toctext">Scala</span></a></li>
<li class="toclevel-2 tocsection-14"><a href="#Bigdata_Modules_and_Dependencies"><span class="tocnumber">5.3</span> <span class="toctext">Bigdata Modules and Dependencies</span></a></li>
</ul>
</li>
</ul>
</div>

<h3><span class="mw-headline" id="Java_Requirements">Java Requirements</span></h3>
<p><a rel="nofollow" class="external text" href="http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html">Java 7</a> is required build Blazegraph. 
</p>
<h4><span class="mw-headline" id="Encoding_Requirements">Encoding Requirements</span></h4>
<p>Errors have been observed using encoding settings other than UTF-8.   <a rel="nofollow" class="external text" href="https://jira.blazegraph.com/browse/BLZG-1383">BLZG-1383</a>.   It is recommended to explicitly pass the encoding settings to the JVM.  For the Oracle/SUN, the settings below are recommended.
</p>
<pre> java ... -Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8 ...
</pre>
<h2><span class="mw-headline" id="Download_the_code">Download the code</span></h2>
<p>- You can download the <a rel="nofollow" class="external text" href="http://sourceforge.net/projects/bigdata/files/bigdata/2.1.4/blazegraph.war/download">WAR</a>, <a rel="nofollow" class="external text" href="http://sourceforge.net/projects/bigdata/files/bigdata/2.1.4/blazegraph.jar/">Executable Jar</a>, or HA installer from the <a rel="nofollow" class="external text" href="https://sourceforge.net/projects/bigdata/">bigdata sourceforge project page</a>.
</p>
<h3><span class="mw-headline" id="GIT">GIT</span></h3>
<p>You can checkout bigdata from GIT.  Older branches and tagged releases have names like <b>BLAZEGRAPH_RELEASE_2_1_0</b>. 
</p><p>Cloning the latest branch:
</p>
<pre>

     git clone -b BLAZEGRAPH_RELEASE_X_Y_Z --single-branch https://github.com/blazegraph/database.git BLAZEGRAPH_RELEASE_X_Y_Z

</pre>
<p>Note, that --single-branch option requires Git v1.7.10, for earlier versions please use:
</p>
<pre>
     git clone -b BLAZEGRAPH_RELEASE_X_Y_Z https://github.com/blazegraph/database.git BLAZEGRAPH_RELEASE_X_Y_Z
</pre>
<p><br />
Tagged releases are available in GIT with tags in the form BLAZEGRAPH_RELEASE_X_Y_X. 
</p>
<pre>    <a rel="nofollow" class="external free" href="https://github.com/blazegraph/database">https://github.com/blazegraph/database</a>
</pre>
<h3><span class="mw-headline" id="Eclipse">Eclipse</span></h3>
<p>For embedded development, we <b>highly</b> recommend checking out bigdata from GIT into Eclipse as its own project.  See <a rel="nofollow" class="external text" href="https://wiki.blazegraph.com/wiki/index.php/MavenNotes#Getting_Started_Developing_with_Eclipse">Maven Notes</a> to get started in Eclipse with Maven.
</p>
<h3><span class="mw-headline" id="Other_Environments">Other Environments</span></h3>
<p>If you checkout the source from GIT, then use <b>./scripts/mavenInstall.sh</b>, to build a local copy.   You can run <b>./scripts/startBlazegraph.sh</b> to run it from a local copy.
</p>
<h2><span class="mw-headline" id="Blazegraph_deployment_models">Blazegraph deployment models</span></h2>
<p>Blazegraph supports several different deployment models (embedded, standalone, replicated, and scale-out). We generally recommend that applications decouple themselves at the REST layer (SPARQL, SPARQL UPDATE, and the REST API).  However, there are applications where an embedded RDF/graph database makes more sense.
</p>
<h3><span class="mw-headline" id="Non-Embedded_Deployment_Models">Non-Embedded Deployment Models</span></h3>
<ul><li> See <a href="/wiki/index.php/NanoSparqlServer" title="NanoSparqlServer">NanoSparqlServer</a> for easy steps on how to deploy a bigdata SPARQL end point + REST API either using an embedded jetty server (same JVM as your application), executable Jar file, or as a WAR (in a servlet container such as tomcat).</li>
<li> See <a href="/wiki/index.php/Using_Bigdata_with_the_OpenRDF_Sesame_HTTP_Server" title="Using Bigdata with the OpenRDF Sesame HTTP Server" class="mw-redirect">Using_Bigdata_with_the_OpenRDF_Sesame_HTTP_Server</a> for the significantly more complicated procedure required to deploy inside of the Sesame WAR.  Note: We do NOT recommend this approach. The Sesame Server does not use the non-blocking query mode of bigdata.  This can significantly limit the query throughput.  However, the <a href="/wiki/index.php/NanoSparqlServer" title="NanoSparqlServer">NanoSparqlServer</a> delivers non-blocking query.</li>
<li> See <a href="/wiki/index.php/CommonProblems" title="CommonProblems">CommonProblems</a> page for a FAQ on common problems and how to fix them.</li></ul>
<h3><span class="mw-headline" id="Embedded_RDF.2FGraph_Database">Embedded RDF/Graph Database</span></h3>
<p>We have implemented the Sesame API over Blazegraph. Sesame is an open source framework for storing, inferencing and querying of RDF data, much like Jena. The best place to start would be to head to <a rel="nofollow" class="external text" href="http://www.openrdf.org/">http://www.openrdf.org openrdf.org</a>, download Sesame, read their User Guide (specifically Chapter 8 - “The Repository API”), and try writing some code using their pre-packaged memory or disk based triple stores. If you have a handle on this you are 90% of the way to being able to use bigdata as an embedded RDF store.
</p>
<h2><span class="mw-headline" id="Running_Bigdata">Running Bigdata</span></h2>
<p>Make sure you are running with the <b>-server</b> JVM option and provide at least several GB of RAM for the embedded database (e.g., -Xmx4G).  You should encounter extremely quick loading and great query performance. If your experience is not satisfactory, please contact us and let us help you get the most out of our product.  Also see <a href="/wiki/index.php/QueryOptimization" title="QueryOptimization">QueryOptimization</a>, <a href="/wiki/index.php/IOOptimization" title="IOOptimization">IOOptimization</a>, and <a href="/wiki/index.php/PerformanceOptimization" title="PerformanceOptimization">PerformanceOptimization</a>.
</p>
<h2><span class="mw-headline" id="Bundling_Bigdata">Bundling Bigdata</span></h2>
<h3><span class="mw-headline" id="Maven">Maven</span></h3>
<p>You can use maven - see <a href="/wiki/index.php/MavenRepository" title="MavenRepository">MavenRepository</a> for the POM.
</p>
<h3><span class="mw-headline" id="Scala">Scala</span></h3>
<p><a rel="nofollow" class="external text" href="http://www.scala-sbt.org/">SBT</a> is a popular and very powerful building tool. In order to add BigData to SBT projects in your build definition you should add:
</p><p>1) bigdata dependency
</p>
<pre>
libraryDependencies ++= Seq(
    &quot;com.bigdata&quot;&#160;% &quot;bigdata&quot;&#160;% bigDataVersion 
)
</pre>
<p>2) Aeveral Maven repositories to resolvers:
</p>
<pre>
  resolvers += &quot;nxparser-repo&quot; at &quot;http://nxparser.googlecode.com/svn/repository/&quot;,

  resolvers += &quot;Bigdata releases&quot; at &quot;http://systap.com/maven/releases/&quot;,

  resolvers += &quot;Sonatype OSS Releases&quot; at &quot;https://oss.sonatype.org/content/repositories/releases&quot;,

  resolvers += &quot;apache-repo-releases&quot; at &quot;http://repository.apache.org/content/repositories/releases/&quot;
</pre>
<h3><span class="mw-headline" id="Bigdata_Modules_and_Dependencies">Bigdata Modules and Dependencies</span></h3>
<p>There are several project modules at this time. Each module bundles all necessary dependencies in its <b>lib</b> subdirectory.
</p>
<ul><li> <b>bigdata</b> (indices, journals, services, etc)</li>
<li> <b>bigdata-rdf</b> (the RDFS++ database)</li>
<li> <b>bigdata-sails</b> (the Sesame integration for the RDFS++ database)</li>
<li> <b>bigdata-jini</b> (jini integration providing for distributed services - this is NOT required for embedded or standalone deployments)</li></ul>
<p>The following dependencies are required only for the scale-out architecture:
</p>
<ul><li> <b>jini</b></li>
<li> <b>zookeeper</b></li></ul>
<p>ICU is required only if you want to take advantage of compressed Unicode sort keys. This is a great feature if you are using Unicode and is available for both scale-up and scale-out deployments. ICU will be used by default if the ICU dependenies are on the classpath. See the com.bigdata.btree.keys package for further notes on ICU and Unicode options. ICU also has an optional JNI library.
</p><p>Removing jini and zookeeper can save you 10M. Removing ICU can save you 30M.
</p><p>The fastutils dependency is quite large, but it is automatically pruned in our WAR release to just those classes that bigdata actually uses.
</p>
<!-- 
NewPP limit report
CPU time usage: 0.020 seconds
Real time usage: 0.018 seconds
Preprocessor visited node count: 90/1000000
Preprocessor generated node count: 140/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
-->

<!-- Saved in parser cache with key bigdata_wiki:pcache:idhash:11091-0!*!0!!en!*!* and timestamp 20200212030911 and revision id 16817
 -->
</div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://wiki.blazegraph.com/wiki/index.php?title=Installation_guide&amp;oldid=16817">https://wiki.blazegraph.com/wiki/index.php?title=Installation_guide&amp;oldid=16817</a>"</div>
					<!-- category links are disabled
					<div id='catlinks' class='catlinks catlinks-allhidden'></div>					-->
					<!-- end content -->
										<div class="visualClear"></div>
				</div>
			</div>
		</div>
		<div class="visualClear"></div>
					<div id="footer" role="contentinfo"
			>
						<div id="f-poweredbyico">
									<a href="//www.mediawiki.org/"><img src="/wiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>
							</div>
					<ul id="f-list">
									<li id="lastmod"> This page was last modified on 10 October 2017, at 15:29.</li>
									<li id="viewcount">This page has been accessed 7 times.</li>
									<li id="privacy"><a href="/wiki/index.php/Blazegraph:Privacy_policy" title="Blazegraph:Privacy policy">Privacy policy</a></li>
									<li id="about"><a href="/wiki/index.php/Blazegraph:About" title="Blazegraph:About">About Blazegraph</a></li>
									<li id="disclaimer"><a href="/wiki/index.php/Blazegraph:General_disclaimer" title="Blazegraph:General disclaimer">Disclaimers</a></li>
							</ul>
		</div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>if(window.mw){
mw.loader.state({"site":"ready","user":"ready","user.groups":"ready"});
}</script>
<script>if(window.mw){
mw.loader.load(["mediawiki.toc","mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest","ext.FontIon"],null,true);
}</script>
<script>if(window.mw){
mw.config.set({"wgBackendResponseTime":184});
}</script></body></html>		</div>

</div>
<!-- /container -->
<footer class="footer text-center">
	<p>&copy; Systap, 2015</p>
</footer>
