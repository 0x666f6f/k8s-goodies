# Instructions for K8S installation on local Centos machine.

## MICROK8S

0: Optional - use a Centos image in a Docker container to obtain installation files for offline system.Note that the container needs to be running systemd to use the Snap package manager.  
  $ `docker run --rm -itd --privileged --volume=$(pwd):/mnt --name=centos centos/systemd:latest`
  or, alternatively, bypasses the need for the '--privileged' flag which is a security risk, and also linked to a bug when optionally installing/testing Snap packages into containers:  
    `docker run --rm -itd --name=centos --tmpfs /tmp --tmpfs /run --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro --volume=$(pwd):/mnt centos/systemd:latest`  
  $ `docker exec -it centos /bin/bash`  

1: Install snap (enable epel-release repository; install)  
- On CentOS 7, as root  
  $ `yum install -y epel-release`  
  $ `yum install -y snapd`  
- Enable the snapd socket (creates a symlink /etc/systemd/system/sockets.target.wants/snapd.socket to /usr/lib/systemd/system/snapd.socket.)  
  $ `systemctl enable --now snapd.socket`  
- Enable snap in 'classic' mode.  
  $ `ln -s /var/lib/snapd/snap /snap`
- On Amazon Linux, follow these instructions: https://github.com/albuild/snap

2: Install MicroK8s  

- With a standard internet connection, simply install MicroK8s using Snap:
  $ `snap install microk8s --classic`  

- With an airgapped installation, install by collecting requirements via an internet-connected system, transfer them to the offline system and install.

  - On an internet-connected machine (or Docker container 'centos') with snap installed, download snaps and their *.assert files:
  $ `cd /mnt`
  $ `snap download core`  # Include core to obtain base dependencies for downloaded snaps.
  $ `snap download core18`  # Only needed if intending to use Multipass for VM management.
  $ `snap download microk8s`  
  
  - On the offline system:  
    - Transfer the files to the offline target system.  
    - Verify and Install  
      $ `sudo SNAPD_AUTH_DATA_FILENAME=/nonexistant snap ack microk8s*.assert`  
      $ `sudo SNAPD_AUTH_DATA_FILENAME=/nonexistant snap install microk8s*.snap --classic`  


## MINIKUBE
> References: https://vocon-it.com/2018/11/19/single-node-kubernetes-cluster-1-installing-minikube-on-centos/

1: Downloads
  $ `KUBE_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)`  
  $ `curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/amd64/kubectl`  
  $ `curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64`  

  For installing KVM:
  # Install libvirt and qemu-kvm on your system  
  $ `sudo yum install libvirt-daemon-kvm qemu-kvm`  

  # Add yourself to the libvirt group so you don't need to sudo  
  $ `sudo usermod -a -G libvirt $(whoami)`  

  # Update your current session for the group change to take effect  
  $ `newgrp libvirt`  

  Then install the driver itself:  
  $ `curl -Lo docker-machine-driver-kvm2 https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 \  
    && chmod +x docker-machine-driver-kvm2 \  
    && sudo cp docker-machine-driver-kvm2 /usr/local/bin/ \  
    && rm docker-machine-driver-kvm2`  
  
Install Kubectl  
  
*: Installation on target system:  
- Install Docker  
- Optional - Install virtualisation hypervisor  
- Install kubectl and Minikube  
  $ `chmod +x kubectl`  
  $ `mv -f kubectl /usr/local/bin/`  
  $ `install minikube-linux-amd64 /usr/local/bin/minikube`  
  
  Test  
  $ `kubectl version`  
  $ `minikube version`  
  
  Start (without hypervisor)  
  $ `minikube start --vm-driver=none`


## RANCHER
Process to install on an air-gapped instance using the "Kubernetes Install" method.
NB! UNTESTED! This is a draft process.

### Online machine
- Obtain necessary binaries. These need to be executable and available on the $PATH of the target platform.
  - kubectl: `snap download kubectl`, or
    $ `KUBE_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)`  
    $ `curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/amd64/kubectl`
  - helm3: `snap download helm3`, or
    $ `wget https://get.helm.sh/helm-v3.0.3-linux-amd64.tar.gz`  
    $ `wget https://get.helm.sh/helm-v3.0.3-linux-amd64.tar.gz.sha256`  
  - rke:
    $ `wget https://github.com/rancher/rke/releases/download/v1.0.4/rke_linux-amd64`  
    $ `wget https://github.com/rancher/rke/releases/download/v1.0.4/sha256sum.txt`
- Obtain necessary Rancher Docker images using scripts on the Releases page (https://github.com/rancher/rancher/releases). This step necessitates adding 'cert-manager' to rancher-images.txt, which requires a functioning helm3 binary. (If snap is available, the helm commands be executed with `snap run helm3 <foo>`.)  
  $ `wget https://github.com/rancher/rancher/releases/download/v2.3.5/rancher-images.txt`  
  $ `wget https://github.com/rancher/rancher/releases/download/v2.3.5/rancher-load-images.sh`  
  $ `wget https://github.com/rancher/rancher/releases/download/v2.3.5/rancher-save-images.sh`  
  $ `helm repo add jetstack https://charts.jetstack.io`  
  $ `helm repo update`  
  $ `helm fetch jetstack/cert-manager --version v0.9.1`  
  $ `helm template ./cert-manager-v0.9.1.tgz | grep -oP '(?<=image: ").*(?=")' >> ./rancher-images.txt`  
  $ `sort -u rancher-images.txt -o rancher-images.txt`  
  $ `chmod +x rancher-save-images.sh`  
  $ `./rancher-save-images.sh --image-list ./rancher-images.txt`  
- Obtain Helm files  
  $ `helm init -c`  # Note that this errored, and ultimately wasn't necessary, when I tested it.  
  $ `helm repo add rancher-latest https://releases.rancher.com/server-charts/latest`  
  $ `helm fetch rancher-latest/rancher`  # Creates rancher-2.3.5.tgz  
- Obtain Rancher configuration files
  $ `helm template cert-manager ./cert-manager-v0.9.1.tgz --output-dir .    --namespace cert-manager    --set image.repository=localhost:5000/quay.io/jetstack/cert-manager-controller    --set webhook.image.repository=localhost:5000/quay.io/jetstack/cert-manager-webhook    --set cainjector.image.repository=localhost:5000/quay.io/jetstack/cert-manager-cainjector`  # Creates ./cert-manager/* config files. Ensure that localhost:5000 is changed as appropriate for registry location in airgapped cluster.
  $ `curl -L -o cert-manager/cert-manager-crd.yaml https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml`  # Download the CRD file.
  $ `helm template rancher ./rancher-2.3.5.tgz --output-dir . --namespace cattle-system  --set hostname=rancher.my-domain --set certmanager.version=0.9.1 --set rancherImage=localhost:5000/rancher/rancher  --set systemDefaultRegistry=localhost:5000 --set useBundledSystemChart=true`  # Creates ./rancher/* config files.
- Also save the registry:2 image as a separate archived object. (This is packed into the ranger-save-images.sh outout file, but I can't figure out how to extract it independently of all the rest to a local registry.)  
  $ `docker pull registry:2`  
  $ `docker save -o registry_2.image registry:2`  

### Offline machine (after transfer of downloaded files)
- Unpack, add to $PATH, and make executable the kubectl, helm3 and rke binaries.
- Instantiate private Docker image registry (or configure access to existing one). 
  - Add this to your local configuration: `/etc/docker/daemon.json` or `/etc/containers/registries.conf`:  `{ "insecure-registries" : ["localhost:5000"] }`, then restart docker:  
    $ `sudo systemctl restart docker`
  - Now load the image into the local registry and instantiate the private registry on localhost.  
    $ `docker load registry_2.image`  
    $ `docker run -d -p 5000:5000 --restart=always --name registry registry:2`  
- Now unpack all images into the registry:
  $ `./rancher-load-images.sh --image-list ./rancher-images.txt --registry localhost:5000`
- Install K8s using RKE (optional - alternatively, use microk8s?)
  $ `rke up --config ./rancher-cluster.yml`
- Install Rancher
  $ `kubectl create namespace cattle-system`  
  $ `kubectl -n cattle-system apply -R -f ./rancher`


## KUBEFLOW
### Online machine
- Download
  $ `wget https://github.com/kubeflow/kubeflow/releases/download/v0.7.1/kfctl_v0.7.1-2-g55f9b2a_linux.tar.gz`
  $ `wget https://raw.githubusercontent.com/kubeflow/manifests/v0.7-branch/kfdef/kfctl_k8s_istio.0.7.1.yaml`

### Offline machine
- Set up K8s
- Enable dynamic provisioning (eg. rancher/local-path-provisioner), and ensure that the StorageClass used by this provisioner is the default StorageClass. 
- Install and configure Kubeflow:  
  - Install downloaded files  
    $ `tar -xvf kfctl_v0.7.1_<platform>.tar.gz`  
  - Configure  
    $ `export PATH=$PATH:"<path-to-kfctl>"`  
    $ `export KF_NAME=<your choice of name for the Kubeflow deployment>`
    $ `export BASE_DIR=<path to a base directory for Kubeflow deployments, eg /opt/>`
    $ `export KF_DIR=${BASE_DIR}/${KF_NAME}`
    $ `export CONFIG_URI="https://raw.githubusercontent.com/kubeflow/manifests/v0.7-branch/kfdef/kfctl_k8s_istio.0.7.1.yaml"`
  - Enable access to dashboard
    $ `export NAMESPACE=istio-system`  
    $ `kubectl port-forward -n istio-system svc/istio-ingressgateway 8080:80`


## SELDON (Core)  
Seldon requires K8s>=1.12 and Helm3.  
- Install Helm3  
- Clone seldon-core repository  
  $ `git clone https://github.com/SeldonIO/seldon-core.git`  
- Install Ingress Gateway (Ambassador / IstIO)
  - If using Ambasssador, install:
  $ `helm repo add stable https://kubernetes-charts.storage.googleapis.com/`  # Add the 'stable' repository to Helm.
  $ `helm repo update`
  $ `helm install ambassador stable/ambassador --set crds.keep=false`
  $ `kubectl rollout status deployment.apps/ambassador`
  $ `kubectl port-forward $(kubectl get pods -n seldon -l app.kubernetes.io/name=ambassador -o jsonpath='{.items[0].metadata.name}') -n seldon 8003:8080`
  - If using Istio
    $ `curl -L https://istio.io/downloadIstio | sh -`
    Installation YAML files for Kubernetes in ./istio-1.4.4/install/kubernetes.
    $ `cd ./istio-1.4.4/bin`  # Add client binary to PATH
    $ `export PATH=$PWD/bin:$PATH
    $ `istioctl manifest apply --set profile=demo`  # The Demo profile is not suitable for performance. It's set up for functionality demonstrations and high level logging.
    

- Install Seldon Core  
  - Option 1:  
    Change the following commands to refer to seldon-charts in the local cloned seldon-core repository.  
    $ `kubectl create namespace seldon-system`  
    $ `helm install seldon-core seldon-core-operator --repo https://storage.googleapis.com/seldon-charts --set usageMetrics.enabled=true --namespace seldon-system`  
    or, with cert-manager  
    $ `helm install seldon-core seldon-core-operator --repo https://storage.googleapis.com/seldon-charts --set usageMetrics.enabled=true --namespace seldon-system --version 0.5.0-SNAPSHOT --set certManager.enabled=true`

  - Option 2, with Kubeflow integration.  
    $ `kubectl label namespace my-namespace serving.kubeflow.org/inferenceservice=enabled`  
    Then create a gateway called kubeflow-gateway in namespace my-namespace using kubectl and kubeflow-gateway.yml. 

  - Option 3
    $ `cd <seldon-core repo>`
    $ `kubectl create namespace seldon
    $ `kubens seldon`
    $ `kubectl create clusterrolebinding kube-system-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:default$ helm install ./helm-charts/seldon-core-crd --name seldon-core-crd --set usage_metrics.enabled=true`
    $ `helm install ./helm-charts/seldon-core --name seldon-core --namespace seldon  --set ambassador.enabled=true`
    Then run a proxy for the Ambassador service:
    $ `kubectl port-forward $(kubectl get pods -n seldon -l service=ambassador -o jsonpath='{.items[0].metadata.name}') -n seldon 8003:8080`

  - Option 4, using Ambassador
    $ `helm install seldon-core seldon-core-operator --repo https://storage.googleapis.com/seldon-charts --set ambassador.enabled=true --set usageMetrics.enabled=true --namespace seldon-system`

  - Option 5, ising IstIO
    $ `helm install seldon-core seldon-core-operator --repo https://storage.googleapis.com/seldon-charts --set istio.enabled=true --set usageMetrics.enabled=true --namespace seldon-system`


## ARGO
### Online
- Download config
  $ `wget https://raw.githubusercontent.com/argoproj/argo/stable/manifests/install.yaml`

### Offline
- Install
  $ `kubectl create namespace argo`
  $ `kubectl apply -n argo -f install.yaml`